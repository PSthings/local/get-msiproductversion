# Get-MSIProductVersion

Function to get MSI Software version numbers from .msi Installer file

#### Usage:

Load the function in PowerShell or Add it to your local Windows PowerShell profile to load the function at startup